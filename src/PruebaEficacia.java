import java.util.Arrays;
import java.util.Random;

public class PruebaEficacia extends MetodosOrdenamiento{

	public static void main(String[] args) {
		
		long tInicio;
		long tFin;
		
		//1000
		int numerosM[] = new int [1000];
		Random r2 = new Random();
		for (int i = 0; i < numerosM.length; i++) {
			
			numerosM[i] = r2.nextInt(100);
		}
		int n2 = numerosM.length;
		int ar2[] = Arrays.copyOf(numerosM, numerosM.length);
		
		//10000
		int numerosDM[] = new int [10000];
		Random r3 = new Random();
		for (int i = 0; i < numerosDM.length; i++) {			
			numerosDM[i] = r3.nextInt(100);
		}
		int n3 = numerosDM.length;
		int ar3[] = Arrays.copyOf(numerosDM, numerosDM.length);
		
		//100000
		int numerosCM[] = new int [100000];
		Random r4 = new Random();
		for (int i = 0; i < numerosCM.length; i++) {
			
			numerosCM[i] = r4.nextInt(100);
		}
		int n4 = numerosCM.length;
		int ar4[] = Arrays.copyOf(numerosCM, numerosCM.length);
		
		//1000000
		int numerosUM[] = new int [1000000];
		Random r5 = new Random();
		for (int i = 0; i < numerosUM.length; i++) {
			
			numerosUM[i] = r4.nextInt(100);
		}
		int n5 = numerosCM.length;
		int ar5[] = Arrays.copyOf(numerosUM, numerosUM.length);
		
		//============================
		tInicio = tFin = 0;

		MetodosOrdenamiento bu = new MetodosOrdenamiento();
		
		//1000
		
		System.out.println("1000");

		System.out.println("==================== BURBUJA 1 ====================");
		System.out.print("Vector Desordenado: ");
		bu.impresion(ar2);
		System.out.print("Vector Ordenado: ");
		bu.ordenarBurbujaUno(ar2);
	
		System.out.println("==================== BURBUJA 2 ====================");
		System.out.print("Vector Desordenado: ");
		bu.impresion(ar2);
		System.out.print("Vector Ordenado: ");
		bu.ordenarBurbujaDos(ar2);
		
		System.out.println("==================== BURBUJA 3 ====================");
		System.out.print("Vector Desordenado: ");
		bu.impresion(ar2);
		System.out.print("Vector Ordenado: ");
		bu.ordenarBurbujaTres(ar2);
		
		System.out.println("==================== QUICK SORT ====================");
		System.out.print("Vector Desordenado: ");
		bu.impresion(ar2);
		System.out.print("Vector Ordenado: ");
		bu.clean();
		tInicio = System.nanoTime();
		bu.quickSort(ar2,0,ar2.length-1);
		tFin = System.nanoTime();
		bu.impresion(ar2);
		bu.informacionEjecucion();
		System.out.println("Tiempo: "+(tFin-tInicio));
		tInicio = tFin =0;
		bu.clean();
		
		System.out.println("==================== SHELL SORT ====================");
		System.out.print("Vector Desordenado: ");
		bu.impresion(ar2);
		System.out.print("Vector Ordenado: ");
		bu.shellSort(ar2);
		
		System.out.println("==================== RADIX SORT ====================");
		System.out.print("Vector Desordenado: ");
		bu.impresion(ar2);
		System.out.print("Vector Ordenado: ");
		bu.radixSort(ar2);
		
		System.out.println("==================== INTERCALACION ====================");
		System.out.print("Vectores Desordenados: \n");
		bu.impresion(ar2);
		bu.impresion(ar2);
		System.out.print("Vector Ordenado: ");
		bu.intercalacion(ar2,ar2);
		
		System.out.println("==================== MEZCLA DIRECTA ====================");
		System.out.print("Vector Desordenado: ");
		bu.impresion(ar2);
		System.out.print("Vector Ordenado: ");
		tInicio = System.nanoTime();
		bu.mezclaDirecta(ar2);
		tFin = System.nanoTime();
		bu.impresion(ar2);
		bu.informacionEjecucion();
		System.out.println("Tiempo: "+(tFin-tInicio));
		bu.clean();
		
		System.out.println("==================== MEZCLA NATURAL ====================");
		System.out.print("Vectores Desordenados: ");
		bu.impresion(ar2);
		System.out.print("Vector Ordenado: ");
		bu.mezclaNatural(ar2);
		
		//10000
		
		System.out.println("10000");

		System.out.println("==================== BURBUJA 1 ====================");
		System.out.print("Vector Desordenado: ");
		bu.impresion(ar3);
		System.out.print("Vector Ordenado: ");
		bu.ordenarBurbujaUno(ar3);
	
		System.out.println("==================== BURBUJA 2 ====================");
		System.out.print("Vector Desordenado: ");
		bu.impresion(ar3);
		System.out.print("Vector Ordenado: ");
		bu.ordenarBurbujaDos(ar3);
		
		System.out.println("==================== BURBUJA 3 ====================");
		System.out.print("Vector Desordenado: ");
		bu.impresion(ar3);
		System.out.print("Vector Ordenado: ");
		bu.ordenarBurbujaTres(ar3);
		
		System.out.println("==================== QUICK SORT ====================");
		System.out.print("Vector Desordenado: ");
		bu.impresion(ar3);
		System.out.print("Vector Ordenado: ");
		bu.clean();
		tInicio = System.nanoTime();
		bu.quickSort(ar3,0,ar3.length-1);
		tFin = System.nanoTime();
		bu.impresion(ar3);
		bu.informacionEjecucion();
		System.out.println("Tiempo: "+(tFin-tInicio));
		tInicio = tFin =0;
		bu.clean();
		
		System.out.println("==================== SHELL SORT ====================");
		System.out.print("Vector Desordenado: ");
		bu.impresion(ar3);
		System.out.print("Vector Ordenado: ");
		bu.shellSort(ar3);
		
		System.out.println("==================== RADIX SORT ====================");
		System.out.print("Vector Desordenado: ");
		bu.impresion(ar3);
		System.out.print("Vector Ordenado: ");
		bu.radixSort(ar3);
		
		System.out.println("==================== INTERCALACION ====================");
		System.out.print("Vectores Desordenados: \n");
		bu.impresion(ar3);
		bu.impresion(ar3);
		System.out.print("Vector Ordenado: ");
		bu.intercalacion(ar3,ar3);
		
		System.out.println("==================== MEZCLA DIRECTA ====================");
		System.out.print("Vector Desordenado: ");
		bu.impresion(ar3);
		System.out.print("Vector Ordenado: ");
		tInicio = System.nanoTime();
		bu.mezclaDirecta(ar3);
		tFin = System.nanoTime();
		bu.impresion(ar3);
		bu.informacionEjecucion();
		System.out.println("Tiempo: "+(tFin-tInicio));
		bu.clean();
		
		System.out.println("==================== MEZCLA NATURAL ====================");
		System.out.print("Vectores Desordenados: ");
		bu.impresion(ar3);
		System.out.print("Vector Ordenado: ");
		bu.mezclaNatural(ar3);
		
		//100000
		
		System.out.println("100000");

		System.out.println("==================== BURBUJA 1 ====================");
		System.out.print("Vector Desordenado: ");
		bu.impresion(ar4);
		System.out.print("Vector Ordenado: ");
		bu.ordenarBurbujaUno(ar4);
	
		System.out.println("==================== BURBUJA 2 ====================");
		System.out.print("Vector Desordenado: ");
		bu.impresion(ar4);
		System.out.print("Vector Ordenado: ");
		bu.ordenarBurbujaDos(ar4);
		
		System.out.println("==================== BURBUJA 3 ====================");
		System.out.print("Vector Desordenado: ");
		bu.impresion(ar4);
		System.out.print("Vector Ordenado: ");
		bu.ordenarBurbujaTres(ar4);
		
		System.out.println("==================== QUICK SORT ====================");
		System.out.print("Vector Desordenado: ");
		bu.impresion(ar4);
		System.out.print("Vector Ordenado: ");
		bu.clean();
		tInicio = System.nanoTime();
		bu.quickSort(ar4,0,ar4.length-1);
		tFin = System.nanoTime();
		bu.impresion(ar4);
		bu.informacionEjecucion();
		System.out.println("Tiempo: "+(tFin-tInicio));
		tInicio = tFin =0;
		bu.clean();
		
		System.out.println("==================== SHELL SORT ====================");
		System.out.print("Vector Desordenado: ");
		bu.impresion(ar4);
		System.out.print("Vector Ordenado: ");
		bu.shellSort(ar4);
		
		System.out.println("==================== RADIX SORT ====================");
		System.out.print("Vector Desordenado: ");
		bu.impresion(ar4);
		System.out.print("Vector Ordenado: ");
		bu.radixSort(ar4);
		
		System.out.println("==================== INTERCALACION ====================");
		System.out.print("Vectores Desordenados: \n");
		bu.impresion(ar4);
		bu.impresion(ar4);
		System.out.print("Vector Ordenado: ");
		bu.intercalacion(ar4,ar4);
		
		System.out.println("==================== MEZCLA DIRECTA ====================");
		System.out.print("Vector Desordenado: ");
		bu.impresion(ar4);
		System.out.print("Vector Ordenado: ");
		tInicio = System.nanoTime();
		bu.mezclaDirecta(ar4);
		tFin = System.nanoTime();
		bu.impresion(ar4);
		bu.informacionEjecucion();
		System.out.println("Tiempo: "+(tFin-tInicio));
		bu.clean();
		
		System.out.println("==================== MEZCLA NATURAL ====================");
		System.out.print("Vectores Desordenados: ");
		bu.impresion(ar4);
		System.out.print("Vector Ordenado: ");
		bu.mezclaNatural(ar4);
		
		//1000000
		
		System.out.println("1000000");

		System.out.println("==================== BURBUJA 1 ====================");
		System.out.print("Vector Desordenado: ");
		bu.impresion(ar5);
		System.out.print("Vector Ordenado: ");
		bu.ordenarBurbujaUno(ar5);
	
		System.out.println("==================== BURBUJA 2 ====================");
		System.out.print("Vector Desordenado: ");
		bu.impresion(ar5);
		System.out.print("Vector Ordenado: ");
		bu.ordenarBurbujaDos(ar5);
		
		System.out.println("==================== BURBUJA 3 ====================");
		System.out.print("Vector Desordenado: ");
		bu.impresion(ar5);
		System.out.print("Vector Ordenado: ");
		bu.ordenarBurbujaTres(ar5);
		
		System.out.println("==================== QUICK SORT ====================");
		System.out.print("Vector Desordenado: ");
		bu.impresion(ar5);
		System.out.print("Vector Ordenado: ");
		bu.clean();
		tInicio = System.nanoTime();
		bu.quickSort(ar5,0,ar5.length-1);
		tFin = System.nanoTime();
		bu.impresion(ar5);
		bu.informacionEjecucion();
		System.out.println("Tiempo: "+(tFin-tInicio));
		tInicio = tFin =0;
		bu.clean();
		
		System.out.println("==================== SHELL SORT ====================");
		System.out.print("Vector Desordenado: ");
		bu.impresion(ar5);
		System.out.print("Vector Ordenado: ");
		bu.shellSort(ar5);
		
		System.out.println("==================== RADIX SORT ====================");
		System.out.print("Vector Desordenado: ");
		bu.impresion(ar5);
		System.out.print("Vector Ordenado: ");
		bu.radixSort(ar5);
		
		System.out.println("==================== INTERCALACION ====================");
		System.out.print("Vectores Desordenados: \n");
		bu.impresion(ar5);
		bu.impresion(ar5);
		System.out.print("Vector Ordenado: ");
		bu.intercalacion(ar5,ar5);
		
		System.out.println("==================== MEZCLA DIRECTA ====================");
		System.out.print("Vector Desordenado: ");
		bu.impresion(ar5);
		System.out.print("Vector Ordenado: ");
		tInicio = System.nanoTime();
		bu.mezclaDirecta(ar5);
		tFin = System.nanoTime();
		bu.impresion(ar5);
		bu.informacionEjecucion();
		System.out.println("Tiempo: "+(tFin-tInicio));
		bu.clean();
		
		System.out.println("==================== MEZCLA NATURAL ====================");
		System.out.print("Vectores Desordenados: ");
		bu.impresion(ar5);
		System.out.print("Vector Ordenado: ");
		bu.mezclaNatural(ar5);
		
		
	}

}

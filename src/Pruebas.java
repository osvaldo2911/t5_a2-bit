import java.util.Random;

public class Pruebas {

	@SuppressWarnings("static-access")
	
	public static void main(String[] args) {
		//Vector random

		Random r = new Random(); 

		int vector[] = new int[10];
		for (int i = 0; i < vector.length; i++) {
			vector[i]= r.nextInt(100);
		}
		
		int vectorBurbuja1[] = vector.clone();
		int vectorBurbuja2[] = vector.clone();
		int vectorBurbuja3[] = vector.clone();
		int quickSort[] = vector.clone();
		int shellSort[] = vector.clone();
		int radixSort[] = vector.clone();
		
		int vectorA[] = new int[5];
		for (int i = 0; i < vectorA.length; i++) {
			vectorA[i]= r.nextInt(100);
		}
		
		int vectorB[] = new int[5];
		for (int i = 0; i < vectorB.length; i++) {
			vectorB[i]= r.nextInt(100);
		}
		
		int mezclaDirecta[] = vector.clone();
		int mezclaNatural[] = vector.clone();
		int busquedaSYB[] = vector.clone();
		
		//Vector random

		MetodosOrdenamiento bu = new MetodosOrdenamiento();

		System.out.println("==================== BURBUJA 1 ====================");
		System.out.print("Vector Desordenado: ");
		bu.impresion(vectorBurbuja1);
		System.out.print("Vector Ordenado: ");
		bu.ordenarBurbujaUno(vectorBurbuja1);
	
		System.out.println("==================== BURBUJA 2 ====================");
		System.out.print("Vector Desordenado: ");
		bu.impresion(vectorBurbuja2);
		System.out.print("Vector Ordenado: ");
		bu.ordenarBurbujaDos(vectorBurbuja2);
		
		System.out.println("==================== BURBUJA 3 ====================");
		System.out.print("Vector Desordenado: ");
		bu.impresion(vectorBurbuja3);
		System.out.print("Vector Ordenado: ");
		bu.ordenarBurbujaTres(vectorBurbuja3);
		
		System.out.println("==================== QUICK SORT ====================");
		System.out.print("Vector Desordenado: ");
		bu.impresion(quickSort);
		System.out.print("Vector Ordenado: ");
		bu.clean();
		long tInicio = System.nanoTime();
		bu.quickSort(quickSort,0,quickSort.length-1);
		long tFin = System.nanoTime();
		bu.impresion(quickSort);
		bu.informacionEjecucion();
		System.out.println("Tiempo: "+(tFin-tInicio));
		tInicio = tFin =0;
		bu.clean();
		
		System.out.println("==================== SHELL SORT ====================");
		System.out.print("Vector Desordenado: ");
		bu.impresion(shellSort);
		System.out.print("Vector Ordenado: ");
		bu.shellSort(shellSort);
		
		System.out.println("==================== RADIX SORT ====================");
		System.out.print("Vector Desordenado: ");
		bu.impresion(radixSort);
		System.out.print("Vector Ordenado: ");
		bu.radixSort(radixSort);
		
		System.out.println("==================== INTERCALACION ====================");
		System.out.print("Vectores Desordenados: \n");
		bu.impresion(vectorA);
		bu.impresion(vectorB);
		System.out.print("Vector Ordenado: ");
		bu.intercalacion(vectorA,vectorB);
		
		System.out.println("==================== MEZCLA DIRECTA ====================");
		System.out.print("Vector Desordenado: ");
		bu.impresion(mezclaDirecta);
		System.out.print("Vector Ordenado: ");
		tInicio = System.nanoTime();
		bu.mezclaDirecta(mezclaDirecta);
		tFin = System.nanoTime();
		bu.impresion(mezclaDirecta);
		bu.informacionEjecucion();
		System.out.println("Tiempo: "+(tFin-tInicio));
		bu.clean();
		
		System.out.println("==================== MEZCLA NATURAL ====================");
		System.out.print("Vectores Desordenados: ");
		bu.impresion(mezclaNatural);
		System.out.print("Vector Ordenado: ");
		bu.mezclaNatural(mezclaNatural);
		
		System.out.println("==================== BUSQUEDA SECUENCIAL Y BINARIA ====================");
		
        int n = busquedaSYB.length; 
        int x = 1; 
		
		System.out.print("Vectores Desordenados: ");
		bu.impresion(busquedaSYB);
		System.out.print("Vector Ordenado: ");
		int resultado = bu.busquedaBinaria(busquedaSYB,0,n,x);
		if (resultado == -1) 
            System.out.println("El n�mero " +x+ " no est� en el vector."); 
        else
            System.out.println("N�mero encontrado en la posici�n "+resultado+" del vector."); 
		
		
	}

}

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Menu extends MetodosOrdenamiento{

	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		boolean a = true;
		
		MetodosOrdenamiento ob = new MetodosOrdenamiento();
		
		int numeros[] = new int [100];
		Random r = new Random();
		for (int i = 0; i < numeros.length; i++) {
			
			numeros[i] = r.nextInt(100);
		}
		int n = numeros.length;
		int ar[] = Arrays.copyOf(numeros, numeros.length);
		byte op;
		do {
			numeros = Arrays.copyOf(ar, ar.length);
			System.out.println("==================================");
			System.out.println("Seleccione un metodo de ordenamiento");
			System.out.println("1.- Metodo Burbuja");
			System.out.println("2.- Metodo QuickSort");
			System.out.println("3.- Metodo ShellSort");
			System.out.println("4.- Metodo RadixSort");
			System.out.println("5.- Metodo Intercalacion");
			System.out.println("6.- Metodo Mezcla directa");
			System.out.println("7.- Metodo Mezcla natural");
			System.out.println("8.- Salir");
			System.out.print("Selecciona una opcion: ");
			op = entrada.nextByte();
			System.out.println("==================================");
			switch (op) {
			case 1:
				System.out.println("Ordenando por el metodo Burbuja");
				ob.ordenarBurbujaUno(numeros);
				break;
			case 2:
				System.out.println("Ordenando por el metodo QuickSort");
				System.out.println("Arreglo ordenado");
				ob.quickSort(numeros, 0, n-1);
				break;
			case 3:
				System.out.println("Ordenando por el metodo ShellSort");
				System.out.println("Arreglo ordenado");
				ob.shellSort(numeros);
				break;
			case 4:
				System.out.println("Ordenando por el metodo Radix");
				ob.radixSort(numeros);
				break;
			case 5:
				System.out.println("Ordenando por el metodo Intercalacion");
				System.out.println("Arreglo ordenado");
				ob.intercalacion(numeros, numeros);
				break;
			case 6:
				System.out.println("Ordenando por el metodo Mezcla Directa");
				System.out.println("Arreglo ordenado");
				ob.mezclaDirecta(numeros);
				break;
			case 7:
				System.out.println("Ordenando por el metodo Mezcla Natural");
				System.out.println("Arreglo ordenado");
				ob.mezclaNatural(numeros);
				break;
				
			case 8:
				System.out.println("Saliendo . . . . . . . . . . . .");
				break;
				
			default:
				System.out.println("Incorrecto");
				break;
			}
			
		} while (op != 8);
	}

}

import java.util.Arrays;

class DatosMetodo{
	private int cantidadPasadas;
	private int cantidadComparaciones;
	private int cantidadIntercambios;
	
	public int getCantidadPasadas() {
		return cantidadPasadas;
	}
	public void setCantidadPasadas(int cantidadPasadas) {
		this.cantidadPasadas = cantidadPasadas;
	}
	public int getCantidadComparaciones() {
		return cantidadComparaciones;
	}
	public void setCantidadComparaciones(int cantidadComparaciones) {
		this.cantidadComparaciones = cantidadComparaciones;
	}
	public int getCantidadIntercambios() {
		return cantidadIntercambios;
	}
	public void setCantidadIntercambios(int cantidadIntercambios) {
		this.cantidadIntercambios = cantidadIntercambios;
	}
	
	public void informacionEjecucion() {
		System.out.println("Recorridos: "+this.getCantidadPasadas()+"\nComparaciones: "+this.getCantidadComparaciones()+"\nIntercambios: "+this.getCantidadIntercambios());
	}
	public static void impresion(int vector[]) {
		System.out.println(Arrays.toString(vector));
	}
	public void clean() {
		setCantidadComparaciones(0);
		setCantidadIntercambios(0);
		setCantidadPasadas(0);
	}
}
public class Datos {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	}

}
